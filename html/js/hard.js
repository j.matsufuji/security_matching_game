var ques = [] // 問題番号
var ans = [] // 答え番号

var total = 9; // 問題数
var speed = 100; // カードをめくるスピード

var is1st = true; // クリックしたカードが一枚目か判定するフラグ

var click_ques_index; // クリックした問題の配列のインデックス
var click_ans_index; // クリックした解答の配列のインデックス

var returnSec = 1000;  //めくったカードが元に戻る秒数[ms]

var pair_ques = [];　// ペアになった問題のインデックスを記録
var pair_ans = []; // ペアになった解答のインデックスを記録
var score = 0; //得点
var bonus = 0; // ボーナス得点
var was_correct = false; // 前問正解していたか


var card_width = 150; // 画像の横
var card_height = 210; // 画像の縦
var card_zoom_width = 300; // モーダルウィンドウでzoomしたときの幅



//表面を開く
function omoteOpen(index, card_num) {
    if (card_num == 1) {
        $("#question li:eq(" + index + ") img").attr("src", "./img/question/hard/h" + ques[index] + ".png");
        $("#question li:eq(" + index + ") img").stop().animate({ width: String(card_width) + "px", height: String(card_height) + "px" }, speed);
        $("#question li:eq(" + index + ")").stop().animate({ left: "0" }, speed);
    } else {
        $("#answer li:eq(" + index + ") img").attr("src", "./img/answer/hard/h" + ans[index] + ".png");
        $("#answer li:eq(" + index + ") img").stop().animate({ width: String(card_width) + "px", height: String(card_height) + "px" }, speed);
        $("#answer li:eq(" + index + ")").stop().animate({ left: "0" }, speed);
    }
}

//裏面を開く
function uraOpen(index, card_num) {
    if (card_num == 1) {
        $("#question li:eq(" + index + ") img").attr("src", "./img/card/red.png");
        $("#question li:eq(" + index + ") img").stop().animate({ width: String(card_width) + "px", height: String(card_height) + "px" }, speed);
        $("#question li:eq(" + index + ")").stop().animate({ left: "0" }, speed);
    } else {
        $("#answer li:eq(" + index + ") img").attr("src", "./img/card/blue.png");
        $("#answer li:eq(" + index + ") img").stop().animate({ width: String(card_width) + "px", height: String(card_height) + "px" }, speed);
        $("#answer li:eq(" + index + ")").stop().animate({ left: "0" }, speed);
    }
}

// 問題または解答のカードをクリックできないようにロックする．
// card_num, 1: 問題，2: 解答
function cardlock(index, card_num) {

    if (card_num == 1) {
        $("#question li:eq(" + index + ")").addClass("lock");
    } else {
        $("#answer li:eq(" + index + ")").addClass("lock");
    }
}

//カード1をロック
function questionlock() {
    $("#question li").addClass("lock");
}

//カード2をロック
function answerlock() {
    $("#answer li").addClass("lock");
}

//全てのカードをロック
function alllock() {
    $("#question li").addClass("lock");
    $("#answer li").addClass("lock");
}

// カードをクリックできる状態にする
function unlock() {
    $("#question li").removeClass("lock");
    $("#answer li").removeClass("lock");
}

// カードをめくる動作に使用
function cardClose(index, card_num, n,) {
    if (card_num == 1) {
        $("#question li:eq(" + index + ")").stop().animate({ left: "75" }, speed);
        $("#question li:eq(" + index + ") img").stop().animate({ width: "0", height: String(card_height) + "px" }, speed,
            function () {
                n(index, card_num);
            });
    } else {
        $("#answer li:eq(" + index + ")").stop().animate({ left: "75" }, speed);
        $("#answer li:eq(" + index + ") img").stop().animate({ width: "0", height: String(card_height) + "px" }, speed,
            function () {
                n(index, card_num);
            });
    }

}

// 2枚目をクリックしたときに，ペアになっているか比較
function compare(question_index, answer_index) {
    if (ques[question_index] == ans[answer_index]) {
        // カードが揃っているときの処理
        return true;
    } else {
        // カードが揃っていないときの処理
        return false;
    }
}

function intRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// 全体の処理
$(function () {
    // 保存してある問題数を取得
    // ファイル数を取得するのは難しい？
    var file_num = 9;

    // 問題番号の配列を作成
    // i番目の要素cat_iは，i番目に並んでいて，問題番号はcat_i
    // cat_iがそれぞれ一致したときに成功
    for (i = 1; i <= total; i++) {
        while (true) {
            var tmp = intRandom(1, file_num);
            if (!ques.includes(tmp)) {
                ques.push(tmp);
                ans.push(tmp);
                break;
            }
        }
    }
    // 配列の中身をランダムに並べ替え
    ques.sort(function () {
        return Math.random() - Math.random();
    });
    ans.sort(function () {
        return Math.random() - Math.random();
    });
    //カード画像の入ったliタグの生成
    for (i = 1; i <= total; i++) {
        $("#question").append("<li><img src='./img/card/red.png' width='" + String(card_width) + "'></li>");
        $("#answer").append("<li><img src='img/card/blue.png' width='" + String(card_width) + "'></li>");
    }

    // モーダルウィンドウの画像設定
    $("#zoom").append("<img src='./img/card/red.png' width='"+ card_zoom_width +"'>");

    // 問題をクリックしたときの処理
    $("#question li").click(function () {
        click_ques_index = $("#question li").index(this);  //選択したカードの順番をindexに保存

        // モーダルウィンドウを表示する．
        $("#zoom img").attr("src", "./img/question/hard/h" + ques[click_ques_index] + ".png");
        modal.style.display = 'block';
        window.addEventListener('click', function(e) {
        if (e.target == modal) {
            modal.style.display = 'none';
        }
        });

        questionlock();  //左側カードのクリックを無効にする関数
        cardClose(click_ques_index, 1, omoteOpen);  //カードの表面を開く関数
        // 2枚目のときは比較する．
        if (is1st) {
            is1st = false;
        } else {
            is1st = true;
            result = compare(click_ques_index, click_ans_index);
            if (result) {
                // ペアが成立したときの処理
                if (was_correct) {
                    bonus = bonus + 10;
                }
                was_correct = true;
                pair_ques.push(click_ques_index);
                pair_ans.push(click_ans_index);
                score = score + 10 + bonus;
                document.getElementById('score').innerText = "SCORE: " + score;
                if (pair_ques.length == total) {  //ペアが全て見つかったら
                    setTimeout(function () {  //最後のカードがめくられた後にクリアー表示
                        window.location.href = 'finish.html';
                    }, returnSec);
                }
            } else {
                // ペアになっていなかったときの処理
                was_correct = false;
                bonus = 0;
                // 選択したカードを裏に戻す．
                setTimeout(function () {  //returnSecミリ秒後（カードをめくる動作が終わった後）に
                    cardClose(click_ques_index, 1, uraOpen);
                    cardClose(click_ans_index, 2, uraOpen);
                }, returnSec);

            }
            unlock();
            for (let i = 0; i < pair_ans.length; i++) {
                cardlock(pair_ques[i], 1);
                cardlock(pair_ans[i], 2);
            }
        }

    });

    // 答えをクリックしたときの処理
    $("#answer li").click(function () {
        click_ans_index = $("#answer li").index(this);  //選択したカードの順番をindexに保存

        // モーダルウィンドウを表示する．
        $("#zoom img").attr("src", "./img/answer/hard/h" + ans[click_ans_index] + ".png");
        modal.style.display = 'block';
        window.addEventListener('click', function(e) {
        if (e.target == modal) {
            modal.style.display = 'none';
        }
        });

        answerlock();  //左側カードのクリックを無効にする関数
        cardClose(click_ans_index, 2, omoteOpen);  //カードの表面を開く関数
        // 2枚目のときは比較する．
        if (is1st) {
            is1st = false;
        } else {
            is1st = true;
            result = compare(click_ques_index, click_ans_index);
            if (result) {
                // ペアが成立したときの処理
                if (was_correct) {
                    bonus = bonus + 10;
                }
                was_correct = true;
                pair_ques.push(click_ques_index);
                pair_ans.push(click_ans_index);
                score = score + 10 + bonus;
                document.getElementById('score').innerText = "SCORE: " + score;
                if (pair_ques.length == total) {  //ペアが全て見つかったら
                    setTimeout(function () {  //最後のカードがめくられた後にクリアー表示
                        window.location.href = 'finish.html';
                    }, returnSec);
                }
            } else {
                // ペアになっていなかったときの処理
                was_correct = false;
                bonus = 0;
                // 選択したカードを裏に戻す．
                setTimeout(function () {  //returnSecミリ秒後（カードをめくる動作が終わった後）に
                    cardClose(click_ques_index, 1, uraOpen);
                    cardClose(click_ans_index, 2, uraOpen);
                }, returnSec);
            }
            unlock()
            for (let i = 0; i < pair_ans.length; i++) {
                cardlock(pair_ques[i], 1);
                cardlock(pair_ans[i], 2);
            }
        }
    });

});

